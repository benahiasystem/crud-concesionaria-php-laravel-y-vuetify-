<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cliente;
use PDF;

class reporteController extends Controller
{
    public function clientes()
    {
        $clientes = cliente::where('activo', true)->get(); 
        $pdf = PDF::loadView('reporteCliente',compact('clientes'));
        $pdf->setPaper('a4','landscape');
        return $pdf->stream('Clientes.pdf');
    }

}