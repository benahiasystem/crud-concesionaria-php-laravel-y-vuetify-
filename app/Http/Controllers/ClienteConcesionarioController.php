<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\concesionarios;
use App\cliente;

class ClienteConcesionarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $concesionario = concesionarios::find($id);

        if($concesionario){
        $cliente = $concesionario->cliente;
        return response()->json(['datos'=> $cliente],200);
        }else{
            return response()->json(['mensaje'=> "un error"],422);
        }
        


       // return $cliente = cliente::find(2)->nombre;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $idC)
    {
        $concesionario = concesionarios::find($id)->id;

        if ($concesionario){
        $cliente = cliente::find($idC);
        return response()->json(['datos'=>$cliente],200);
        }else{
            return redirect()->back()
            ->withErrors("un error");
        }
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
