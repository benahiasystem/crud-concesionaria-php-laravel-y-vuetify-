<?php

use GuzzleHttp\Middleware;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'usuarioAdmin'], function (){
    Route::get('clientes', function () {
        return view('cliente');
    })->name('cliente');

    Route::get('concesionarios', function () {
        return view('concesionario');
    })->name('concesionario'); 

    Route::get('reporte/clientes', 'ReporteController@clientes')->name('reporte');

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


